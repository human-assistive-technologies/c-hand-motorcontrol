#include <xc.h>
#include <stdbool.h>
#include "encoders.h"
#include "motor_controls.h"

#define _XTAL_FREQ  85000000UL
#define FCY (_XTAL_FREQ/2)
#include <libpic30.h>

/***********************************/
bool checkingEncoders(uint8_t channel){
    
    uint8_t ENCA[] = {ENC1_A, ENC2_A, ENC5_A, ENC6_A, ENC3_A, ENC4_A};
    uint8_t ENCB[] = {ENC1_B, ENC2_B, ENC5_B, ENC6_B, ENC3_B, ENC4_B};
    
    static uint8_t past_ENC;
    static uint32_t stop;
    static uint32_t pulse;
    static bool ban=true;
    
    uint8_t encoder_Enable=true;
    
    uint8_t now_ENC=ENCB[channel];
    
    if(ban){    
        past_ENC=ENCB[channel]; 
        stop=0;
        pulse=0;
        ban=false;
    }
    
    if(now_ENC != past_ENC){  
        if(!ENCA[channel]){
            pulse++;  
            stop=0;
        }
        past_ENC=now_ENC; 
    }
    stop++; 
    
     if(stop>=COUNTER_STOP){           
            FINGERS_STOP(channel);
            ban=true;
            encoder_Enable=false;            
         
            printf("Finger %d pulse: %ld\r\n",channel,pulse);
    }  
    return encoder_Enable;
}

bool statusEncoder(uint8_t channel){
    
    uint8_t ENCA[] = {ENC1_A, ENC2_A, ENC5_A, ENC6_A, ENC3_A, ENC4_A};
    uint8_t ENCB[] = {ENC1_B, ENC2_B, ENC5_B, ENC6_B, ENC3_B, ENC4_B};
    uint8_t COUNTER[] = {COUNTER_ENC_1,COUNTER_ENC_2,COUNTER_ENC_5,COUNTER_ENC_6,COUNTER_ENC_3,COUNTER_ENC_4};
    
    static uint8_t past_ENC[] = {0,0,0,0,0,0};
    static uint32_t stop[] = {0,0,0,0,0,0};
    static uint32_t DG1[]= {0,0,0,0,0,0};
    static bool ban[]={true,true,true,true,true,true};
    
    uint8_t encoder_Enable=true;
    
    uint8_t now_ENC=ENCB[channel];
    
    if(ban[channel]){
        past_ENC[channel]=ENCB[channel]; 
        ban[channel]=false;
        stop[channel]=0;
        DG1[channel]=COUNTER[channel];
    }
    
    if(now_ENC != past_ENC[channel]){  
        if(!ENCA[channel]){
            DG1[channel]--;  
            stop[channel]=0;
        }
        past_ENC[channel]=now_ENC; 
    }
    stop[channel]++; 
    
     if(stop[channel]==COUNTER_STOP || !DG1[channel]){
            FINGERS_STOP(channel);
//            FINGER1_STOP();
//            FINGER2_STOP();
//            printf("Status finger #1: HOME\r\n");
//            __delay_ms(500);
            ban[channel]=true;
            encoder_Enable=false;
    }  
    
    return encoder_Enable;
}


bool encoderDegree(uint8_t degree,uint8_t channel){
    
    uint8_t ENCA[] = {ENC1_A, ENC2_A, ENC5_A, ENC6_A, ENC3_A, ENC4_A};
    uint8_t ENCB[] = {ENC1_B, ENC2_B, ENC5_B, ENC6_B, ENC3_B, ENC4_B};
    uint8_t COUNTER[] = {COUNTER_ENC_1,COUNTER_ENC_2,COUNTER_ENC_5,COUNTER_ENC_6,COUNTER_ENC_3,COUNTER_ENC_4};
    
    static uint8_t past_ENC[] = {0,0,0,0,0,0};
    static uint32_t stop[] = {0,0,0,0,0,0};
    static uint8_t DG1[]= {0,0,0,0,0,0};
    static bool ban[]={true,true,true,true,true,true};
    
    uint8_t encoder_Enable=true;
    
    uint8_t now_ENC=ENCB[channel];
    
    if(ban[channel]){    
        past_ENC[channel]=ENCB[channel]; 
        stop[channel]=0;
        DG1[channel]=(COUNTER[channel]*degree)/90;
        ban[channel]=false;
        //printf("\r\nFinger %d pulsos: %d\r\n",channel,DG1[channel]);
    }
    
    if(now_ENC != past_ENC[channel]){  
        if(!ENCA[channel]){
            DG1[channel]--;  
            stop[channel]=0;
        }
        past_ENC[channel]=now_ENC; 
    }
    stop[channel]++; 
    
     if(stop[channel]==COUNTER_STOP || !DG1[channel]){           
            FINGERS_STOP(channel);
//            printf("Status finger #1: Arrive\r\n");
//            printf("\r\nFinger %d degree: %d\r\n",channel,COUNTER[channel]);
//            printf("Finger stop: %ld\r\n",stop);
//            __delay_ms(500);
            ban[channel]=true;
            encoder_Enable=false;
    }  
    
    return encoder_Enable;
}

bool encoderProtection(uint8_t channel){
    
    uint8_t ENCA[] = {ENC1_A, ENC2_A, ENC5_A, ENC6_A, ENC3_A, ENC4_A};
    uint8_t ENCB[] = {ENC1_B, ENC2_B, ENC5_B, ENC6_B, ENC3_B, ENC4_B};
//    uint8_t COUNTER[] = {COUNTER_ENC_1,COUNTER_ENC_2,COUNTER_ENC_5,COUNTER_ENC_6,COUNTER_ENC_3,COUNTER_ENC_4};
    
    static uint8_t past_ENC[] = {0,0,0,0,0,0};
    static uint32_t stop[] = {0,0,0,0,0,0};
//    static uint32_t DG[]= {0,0,0,0,0,0};
    static bool ban[]={true,true,true,true,true,true};
    
    uint8_t encoder_Enable=true;
    
    uint8_t now_ENC=ENCB[channel];
    
    if(ban[channel]){    
        past_ENC[channel]=ENCB[channel]; 
        stop[channel]=0;
        ban[channel]=false;
        //printf("\r\nFinger %d pulsos: %d\r\n",channel,DG1[channel]);
    }
    
    if(now_ENC != past_ENC[channel]){  
        if(!ENCA[channel]){
            stop[channel]=0;
        }
        past_ENC[channel]=now_ENC; 
    }
    stop[channel]++; 
    
     if(stop[channel]==COUNTER_STOP){           
            FINGERS_STOP(channel);
            printf("Status finger #%d: Arrive\r\n",channel);
//            printf("\r\nFinger %d degree: %d\r\n",channel,COUNTER[channel]);
//            printf("Finger stop: %ld\r\n",stop);
//            __delay_ms(500);
            ban[channel]=true;
            encoder_Enable=false;
    }  
    
    return encoder_Enable;
}