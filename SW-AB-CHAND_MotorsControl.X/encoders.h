/* 
 * File:   encoders.h
 * Author: fabzm
 *
 * Created on 9 de febrero de 2021, 03:53 PM
 */

#ifndef ENCODERS_H
#define	ENCODERS_H

#include <xc.h>
#include <stdbool.h>

#define ENC6_B   _RB4
#define ENC6_A   _RA8
#define ENC5_B   _RC5
#define ENC5_A   _RC4
#define ENC4_B   _RA1
#define ENC4_A   _RA0
#define ENC3_B   _RC2
#define ENC3_A   _RC1
#define ENC2_B   _RB2
#define ENC2_A   _RB1
#define ENC1_B   _RC0
#define ENC1_A   _RB3
/*Macros*/
#define COUNTER_STOP    30000
#define COUNTER_ENC_1   130
#define COUNTER_ENC_2   125
#define COUNTER_ENC_3   100
#define COUNTER_ENC_4   175
#define COUNTER_ENC_5   130
#define COUNTER_ENC_6   130
#define PpG1 (130/90)
#define PpG2 (COUNTER_ENC_2/90)
#define PpG3 (COUNTER_ENC_3/90)
#define PpG4 (COUNTER_ENC_4/90)
#define PpG5 (COUNTER_ENC_5/90)
#define PpG6 (COUNTER_ENC_6/90)

bool checkingEncoders(uint8_t);
bool statusEncoder(uint8_t);
bool encoderDegree(uint8_t,uint8_t);
bool encoderProtection(uint8_t);


//#ifdef	__cplusplus
//extern "C" {
//#endif
//
//
//
//
//#ifdef	__cplusplus
//}
//#endif

#endif	/* ENCODERS_H */

