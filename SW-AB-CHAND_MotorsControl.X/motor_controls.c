/* 
 * File:   motor_controls.c
 * Company: Human Assistive Technologies
 * Author: Ing. Bryan Trejo Pe�a
 *
 * Created on February 8, 2021
 */

#include <xc.h>
#include "motor_controls.h"
#include "encoders.h"
#include <stdbool.h>

#define _XTAL_FREQ  85000000UL
#define FCY (_XTAL_FREQ/2)
#include <libpic30.h>
/***********************************/

motorStruct motorControl,motorEnableEnc;

/***********************************/
void Motors_Initialize(bool state){
    if(state){
        motorEnable(1,false);
        motorEnable(2,false);
        motorEnable(3,false);
        motorEnable(4,false);
        motorEnable(5,state);
        motorEnable(6,state);
    }
    else{
        motorEnable(1,false);
        motorEnable(2,false);
        motorEnable(3,false);
        motorEnable(4,false);
        motorEnable(5,false);
        motorEnable(6,false);
    }
}

void motorEnable(uint8_t motor_n,bool state){
    switch(motor_n){
        case 1:
            motorControl.motorEnable_1=state;
            break;
        case 2:
            motorControl.motorEnable_2=state;
            break;
        case 3:
            motorControl.motorEnable_3=state;
            break;
        case 4:
            motorControl.motorEnable_4=state;
            break;
        case 5:
            motorControl.motorEnable_5=state;
            break;
        case 6:
            motorControl.motorEnable_6=state;
            break;
    }
}

void C_HAND_OPEN(){
    FINGER1_OPEN();
    FINGER2_OPEN();
    FINGER3_OPEN();
    FINGER4_OPEN();
    FINGER5_OPEN();
    FINGER6_OPEN();
}
void C_HAND_CLOSE(){
    FINGER1_CLOSE();
    FINGER2_CLOSE();
    FINGER3_CLOSE();
    FINGER4_CLOSE();
    FINGER5_CLOSE();
    FINGER6_CLOSE();
}
void C_HAND_STOP(){
    FINGER1_STOP();
    FINGER2_STOP();
    FINGER3_STOP();
    FINGER4_STOP();
    FINGER5_STOP();
    FINGER6_STOP();
}
/********************/
void FINGER1_OPEN(){
    if(motorControl.motorEnable_1){
        DIR1=HIGH;
        PWM1=HIGH;
    }
}
void FINGER2_OPEN(){
    if(motorControl.motorEnable_2){
        DIR2=HIGH;
        PWM2=HIGH;
    }
}
void FINGER3_OPEN(){    
    if(motorControl.motorEnable_3){
        DIR5=HIGH;
        PWM5=HIGH;
    }
}
void FINGER4_OPEN(){
    if(motorControl.motorEnable_4){
        DIR6=HIGH;
        PWM6=HIGH;
    }
}
void FINGER5_OPEN(){
    if(motorControl.motorEnable_5){
        DIR3=HIGH;
        PWM3=HIGH;
    }
}
void FINGER6_OPEN(){
    if(motorControl.motorEnable_6){
        DIR4=HIGH;
        PWM4=HIGH;
    }
}
/*******************/
void FINGER1_CLOSE(){
    if(motorControl.motorEnable_1){
        DIR1=LOW;
        PWM1=HIGH;
    }
}
void FINGER2_CLOSE(){
    if(motorControl.motorEnable_2){
        DIR2=LOW;
        PWM2=HIGH;
    } 
}
void FINGER3_CLOSE(){
    if(motorControl.motorEnable_3){
        DIR5=LOW;
        PWM5=HIGH;
    }
}
void FINGER4_CLOSE(){
    if(motorControl.motorEnable_4){
        DIR6=LOW;
        PWM6=HIGH;
    }
}
void FINGER5_CLOSE(){
    if(motorControl.motorEnable_5){
        DIR3=LOW;
        PWM3=HIGH;
    }
}
void FINGER6_CLOSE(){
    if(motorControl.motorEnable_6){
        DIR4=LOW;
        PWM4=HIGH;
    }
}
/*********************/
void FINGER1_STOP(){
    PWM1=LOW;
}
void FINGER2_STOP(){
    PWM2=LOW;
}
void FINGER3_STOP(){
    PWM5=LOW;
}
void FINGER4_STOP(){
    PWM6=LOW;
}
void FINGER5_STOP(){
    PWM3=LOW;
}
void FINGER6_STOP(){
    PWM4=LOW;
}
/************************/
void FINGERS_STOP(uint8_t channel){
    switch(channel){
        case 0:
            FINGER1_STOP();
            break;
        case 1:
            FINGER2_STOP();
            break;
        case 2:
            FINGER3_STOP();
            break;
        case 3:
            FINGER4_STOP();
            break;
        case 4:
            FINGER5_STOP();
            break;
        case 5:
            FINGER6_STOP();
            break;
        default:
            printf("\r\n No existe motor\r\n");
            break;
    }    
}
void C_HAND_FINGER_OPEN(uint8_t channel){
    switch(channel){
        case 0:
            FINGER1_OPEN();
            break;
        case 1:
            FINGER2_OPEN();
            break;
        case 2:
            FINGER3_OPEN();
            break;
        case 3:
            FINGER4_OPEN();
            break;
        case 4:
            FINGER5_OPEN();
            break;
        case 5:
            FINGER6_OPEN();
            break;
        default:
            printf("\r\n No existe motor\r\n");
            break;
    }
}
void C_HAND_FINGER_CLOSE(uint8_t channel){
    switch(channel){
        case 0:
            FINGER1_CLOSE();
            break;
        case 1:
            FINGER2_CLOSE();
            break;
        case 2:
            FINGER3_CLOSE();
            break;
        case 3:
            FINGER4_CLOSE();
            break;
        case 4:
            FINGER5_CLOSE();
            break;
        case 5:
            FINGER6_CLOSE();
            break;
        default:
            printf("\r\n No existe motor\r\n");
            break;
    }
}

void C_HAND_MOTORS(bool dir,
        uint8_t degree1,
        uint8_t degree2,
        uint8_t degree3,
        uint8_t degree4,
        uint8_t degree5,
        uint8_t degree6){
    
    bool enableModeDump=true;
    bool enableFinger5=false;
    bool enableFinger6=false;
        
    if(degree5>0){     
        enableFinger5=true;   
        FINGER5_CLOSE();
    }
    
    if(degree6>0){
        enableFinger6=true;
        FINGER6_CLOSE();
    }
    while(enableModeDump){
        if(enableFinger5){
            enableFinger5=encoderDegree(degree5,4);        
        }  
        if(enableFinger6){
            enableFinger6=encoderDegree(degree6,5);        
        }    
        
        if(!enableFinger5&&!enableFinger6)
            enableModeDump=false;
        
    }
    C_HAND_STOP();
    
    
    bool enableMode=true;
    bool enableFinger1=false;
    bool enableFinger2=false;
    bool enableFinger3=false;
    bool enableFinger4=false;
    
    if(degree1>0){     
        enableFinger1=true;   
        FINGER1_CLOSE();
    }
    if(degree2>0){
        enableFinger2=true;
        FINGER2_CLOSE();
    }
    if(degree3>0){
        enableFinger3=true;
        FINGER3_CLOSE();
    }
    if(degree4>0){
        enableFinger4=true;
        FINGER4_CLOSE();
    }
    
    while(enableMode){
        if(enableFinger1){
            enableFinger1=encoderDegree(degree1,0);        
        }  
        if(enableFinger2){
            enableFinger2=encoderDegree(degree2,1);        
        }   
        if(enableFinger3){
            enableFinger3=encoderDegree(degree3,2);        
        }   
        if(enableFinger4){
            enableFinger4=encoderDegree(degree4,3);        
        }  
        if(!enableFinger1&&!enableFinger2&&!enableFinger3&&!enableFinger4)
            enableMode=false;
    }
    C_HAND_STOP();
}

void C_HAND_MOTORS_COM(bool dir,
        uint8_t degree1,
        uint8_t degree2,
        uint8_t degree3,
        uint8_t degree4,
        uint8_t degree5,
        uint8_t degree6){
        
    bool enableMode=true;
    bool enableFinger1=false;
    bool enableFinger2=false;
    bool enableFinger3=false;
    bool enableFinger4=false;
    bool enableFinger5=false;
    bool enableFinger6=false;
    
    if(degree1>0 && motorControl.motorEnable_1){     
        enableFinger1=true;   
        FINGER1_CLOSE();
    }
    if(degree2>0 && motorControl.motorEnable_2){
        enableFinger2=true;
        FINGER2_CLOSE();
    }
    if(degree3>0 && motorControl.motorEnable_3){
        enableFinger3=true;
        FINGER3_CLOSE();
    }
    if(degree4>0 && motorControl.motorEnable_4){
        enableFinger4=true;
        FINGER4_CLOSE();
    }        
    if(degree5>0 && motorControl.motorEnable_5){     
        enableFinger5=true;   
        FINGER5_CLOSE();
    }    
    if(degree6>0 && motorControl.motorEnable_6){
        enableFinger6=true;
        FINGER6_CLOSE();
    }
    
    while(enableMode){
        if(enableFinger1){
            enableFinger1=encoderDegree(degree1,0);        
        }  
        if(enableFinger2){
            enableFinger2=encoderDegree(degree2,1);        
        }   
        if(enableFinger3){
            enableFinger3=encoderDegree(degree3,2);        
        }   
        if(enableFinger4){
            enableFinger4=encoderDegree(degree4,3);        
        }  
        if(enableFinger5){
            enableFinger5=encoderDegree(degree5,4);        
        }  
        if(enableFinger6){
            enableFinger6=encoderDegree(degree6,5);        
        }    
        
        if(!enableFinger1&&!enableFinger2&&!enableFinger3&&!enableFinger4&&!enableFinger5&&!enableFinger6)
            enableMode=false;
    }
    C_HAND_STOP();
}

void C_HAND_OPEN_CLOSE_Enc(bool direction){
    
//    static bool 
//    enable_Move1=true,
//    enable_Move2=true,
//    enable_Move3=true,
//    enable_Move4=true,
//    enable_Move5=true,
//    enable_Move6=true;
    
    if( motorControl.motorEnable_1 && motorEnableEnc.motorEnable_1){
        if(direction)FINGER1_OPEN();
        else FINGER1_CLOSE();
    }
    if( motorControl.motorEnable_2 && motorEnableEnc.motorEnable_2){
        if(direction)FINGER2_OPEN();
        else FINGER2_CLOSE();
    }
    if( motorControl.motorEnable_3 && motorEnableEnc.motorEnable_3){
        if(direction)FINGER3_OPEN();
        else FINGER3_CLOSE();
    }
    if( motorControl.motorEnable_4 && motorEnableEnc.motorEnable_4){
        if(direction)FINGER4_OPEN();
        else FINGER4_CLOSE();
    }
    if( motorControl.motorEnable_5 && motorEnableEnc.motorEnable_5){
        if(direction)FINGER5_OPEN();
        else FINGER5_CLOSE();
    }
    if( motorControl.motorEnable_6 && motorEnableEnc.motorEnable_6){
        if(direction)FINGER6_OPEN();
        else FINGER6_CLOSE();
    }
    
    if(motorEnableEnc.motorEnable_1)
        motorEnableEnc.motorEnable_1=encoderProtection(0);  
    if(motorEnableEnc.motorEnable_2)
        motorEnableEnc.motorEnable_2=encoderProtection(1);  
    if(motorEnableEnc.motorEnable_3)
        motorEnableEnc.motorEnable_3=encoderProtection(2);  
    if(motorEnableEnc.motorEnable_4)
        motorEnableEnc.motorEnable_4=encoderProtection(3);  
    if(motorEnableEnc.motorEnable_5)
        motorEnableEnc.motorEnable_5=encoderProtection(4);  
    if(motorEnableEnc.motorEnable_6)
        motorEnableEnc.motorEnable_6=encoderProtection(5);  
          
}
        

void C_HAND_STOP_ENC(void){
    C_HAND_STOP();
    motorEnableEnc.motorEnable_1=motorControl.motorEnable_1;
    motorEnableEnc.motorEnable_2=motorControl.motorEnable_2;
    motorEnableEnc.motorEnable_3=motorControl.motorEnable_3;
    motorEnableEnc.motorEnable_4=motorControl.motorEnable_4;
    motorEnableEnc.motorEnable_5=motorControl.motorEnable_5;
    motorEnableEnc.motorEnable_6=motorControl.motorEnable_6;
}