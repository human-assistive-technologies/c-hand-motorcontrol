/**
  Generated main.c file from MPLAB Code Configurator

  @Company
    Microchip Technology Inc.

  @File Name
    main.c

  @Summary
    This is the generated main.c using PIC24 / dsPIC33 / PIC32MM MCUs.

  @Description
    This source file provides main entry point for system initialization and application code development.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.170.0
        Device            :  dsPIC33EP512GM604
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.61
        MPLAB 	          :  MPLAB X v5.45
*/

/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/
#include "mcc_generated_files/system.h"
#define _XTAL_FREQ  85000000UL
#define FCY (_XTAL_FREQ/2)
#include <libpic30.h>
#include <stdbool.h>
#include "motor_controls.h"
#include "encoders.h"


#define OP  _RB8
#define CL  _RB9

/*
                         Main application
 */

void C_HAND_home(){       
    printf("Starting HOME: ");
    
    bool comingHome=true;
    bool dumpHome=true;
    bool opositorHome=true;
     
    FINGER5_OPEN();    
    while(dumpHome){
        dumpHome=statusEncoder(4);
    } 
    C_HAND_STOP(); 
    
    
    FINGER6_OPEN();
    while(opositorHome){
        opositorHome=statusEncoder(5);
    }
    
    C_HAND_STOP();
    
    FINGER1_OPEN();
    FINGER2_OPEN();
    FINGER3_OPEN();
    FINGER4_OPEN();
    
    bool enableEnc1=true;
    bool enableEnc2=true;
    bool enableEnc3=true;
    bool enableEnc4=true;
    
    while(comingHome){        
        if(enableEnc1)
            enableEnc1=statusEncoder(0); 
        if(enableEnc2)
            enableEnc2=statusEncoder(1);        
        if(enableEnc3)
            enableEnc3=statusEncoder(2);         
        if(enableEnc4)
            enableEnc4=statusEncoder(3); 
        
        if(!enableEnc1&&!enableEnc2&&!enableEnc3&&!enableEnc4)
            comingHome=false;
        
    }    
    C_HAND_STOP();
    
    printf("OK\r\n");
    __delay_ms(100);
}

void C_HAND_postions(uint8_t mode){
    printf("Starting MODE #%d: ",mode);
    switch(mode){
        case 1:
            C_HAND_MOTORS_COM(CLOSE,20,40,60,80,45,45);
//            C_HAND_MOTORS(CLOSE,90,0,0,0,0,0);
            break;
        case 2:
            C_HAND_MOTORS_COM(CLOSE,90,90,90,90,0,60);
            break;
        case 3:
            C_HAND_MOTORS_COM(CLOSE,10,90,90,10,45,60);
            break;
        case 4:
            C_HAND_MOTORS_COM(CLOSE,25,25,25,25,40,60);
            break;
        case 5:
            C_HAND_MOTORS_COM(CLOSE,30,90,90,90,60,60);
            break;
        default:
            break;            
    }
    printf("OK\r\n"); 
    __delay_ms(100);
}

void testingMotor(uint8_t channel){
    while(1){
        printf("CLOSE");
        C_HAND_FINGER_CLOSE(channel);
        while(checkingEncoders(channel));
        C_HAND_STOP();
        __delay_ms(500);
        
        
        printf("OPEN");
        C_HAND_FINGER_OPEN(channel);
        while(checkingEncoders(channel));
        C_HAND_STOP(); 
        __delay_ms(500);
    }
}

void C_HAND_Show(){
    static uint8_t x=1;
    C_HAND_home();
    __delay_ms(200);        
    C_HAND_postions(x);
    __delay_ms(200);
    x++;
    if(x>3)
        x=1;
}

int main(void)
{
    // initialize the device
    SYSTEM_Initialize();
    Motors_Initialize(true);
    
    __delay_ms(1500);
    printf("\nHuman Assistive Technologies\r");__delay_ms(10);
    printf("C-HAND Motors control\r");__delay_ms(10);
    printf("08/02/2021\r\r");__delay_ms(10);
    
//    testingMotor(4);
    
    C_HAND_home();
    __delay_ms(10);
    C_HAND_postions(1);
    __delay_ms(10);
    
    printf("READY\r\n");
    C_HAND_STOP_ENC();
   
    while (1)
    {             
//        if(OP){
//            C_HAND_OPEN_CLOSE_Enc(true);//True open
//        }
//        else if(CL){
//            C_HAND_OPEN_CLOSE_Enc(false);// False close
//        }
//        else{
//            C_HAND_STOP_ENC();
//        }
        C_HAND_Show();
    }
    return 1; 
}
/**
 End of File
*/

