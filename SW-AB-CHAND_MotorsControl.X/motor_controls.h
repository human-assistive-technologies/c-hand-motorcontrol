/* 
 * File:   motors_control.h
 * Author: fabzm
 *
 * Created on 9 de febrero de 2021, 02:53 PM
 */

#ifndef MOTORS_CONTROL_H
#define	MOTORS_CONTROL_H

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>


#define HIGH    1
#define LOW     0
#define OPEN    true
#define CLOSE   false
/*Motores*/
#define DIR6   _LATC6
#define PWM6   _LATC7
#define DIR5   _LATC8
#define PWM5   _LATC9
#define DIR4   _LATB10
#define PWM4   _LATB11
#define DIR3   _LATB12
#define PWM3   _LATB13
#define DIR2   _LATA10
#define PWM2   _LATA7
#define DIR1   _LATB14
#define PWM1   _LATB15

typedef struct{
    bool motorEnable_1;
    bool motorEnable_2;
    bool motorEnable_3;
    bool motorEnable_4;
    bool motorEnable_5;
    bool motorEnable_6;
}motorStruct;
void C_HAND_OPEN_CLOSE_Enc(bool);
void C_HAND_STOP_ENC(void);

void Motors_Initialize(bool);
void motorEnable(uint8_t,bool);

void C_HAND_OPEN(void);
void C_HAND_CLOSE(void);
void C_HAND_STOP(void);

void FINGER1_OPEN(void);
void FINGER2_OPEN(void);
void FINGER3_OPEN(void);
void FINGER4_OPEN(void);
void FINGER5_OPEN(void);
void FINGER6_OPEN(void);

void FINGER1_CLOSE(void);
void FINGER2_CLOSE(void);
void FINGER3_CLOSE(void);
void FINGER4_CLOSE(void);
void FINGER5_CLOSE(void);
void FINGER6_CLOSE(void);

void FINGER1_STOP(void);
void FINGER2_STOP(void);
void FINGER3_STOP(void);
void FINGER4_STOP(void);
void FINGER5_STOP(void);
void FINGER6_STOP(void);
void FINGERS_STOP(uint8_t);

void C_HAND_FINGER_OPEN(uint8_t);
void C_HAND_FINGER_CLOSE(uint8_t);
void C_HAND_MOTORS(bool,uint8_t,uint8_t,uint8_t,uint8_t,uint8_t,uint8_t);
void C_HAND_MOTORS_COM(bool,uint8_t,uint8_t,uint8_t,uint8_t,uint8_t,uint8_t);

//#ifdef	__cplusplus
//extern "C" {
//#endif
//
//
//
//
//#ifdef	__cplusplus
//}
//#endif

#endif	/* MOTORS_CONTROL_H */

